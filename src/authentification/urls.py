from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView
from .views import Utilisateur, TypeUtilisateur

router = routers.SimpleRouter()
router.register('type-utilisateur', TypeUtilisateur, basename='type-utilisateur')
router.register('registration', Utilisateur, basename='registration')

urlpatterns = [
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('',include(router.urls))
]