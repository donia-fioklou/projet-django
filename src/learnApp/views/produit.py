from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from learnApp.models.produit import Produit
from learnApp.serializers.produit import ProduitSerializer
 

class ProduitViewSet(viewsets.ModelViewSet):
    queryset = Produit.objects.all()
    serializer_class = ProduitSerializer  # Replace with the actual serializer for your model

    permission_classes = [IsAuthenticated]