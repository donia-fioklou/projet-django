from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from learnApp.models.materiel import Materiel
from learnApp.serializers.materiel import MaterielSerializer
 

class MaterielViewSet(viewsets.ModelViewSet):
    queryset = Materiel.objects.all()
    serializer_class = MaterielSerializer  # Replace with the actual serializer for your model

    permission_classes = [IsAuthenticated]