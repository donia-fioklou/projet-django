from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from learnApp.models.exercice import Exercice
from learnApp.serializers.exercice import ExerciceSerializer
 

class ExerciceViewSet(viewsets.ModelViewSet):
    queryset = Exercice.objects.all()
    serializer_class = ExerciceSerializer  # Replace with the actual serializer for your model

    permission_classes = [IsAuthenticated]