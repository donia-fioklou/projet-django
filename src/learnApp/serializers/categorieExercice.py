from rest_framework import serializers

from learnApp.models.categorieExercice import CategorieExercice


class CategorieExerciceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategorieExercice
        fields = '__all__'
