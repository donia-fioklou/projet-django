from rest_framework import serializers

from learnApp.models.categorieProduit import CategorieProduit


class CategorieProduitSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategorieProduit
        fields = '__all__'
