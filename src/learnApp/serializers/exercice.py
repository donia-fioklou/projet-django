from rest_framework import serializers

from learnApp.models.exercice import Exercice


class ExerciceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercice
        fields = '__all__'
