from django.db import models


class CategorieExercice(models.Model):
    libelle = models.CharField(max_length=255)

    def __str__(self):
        return self.libelle