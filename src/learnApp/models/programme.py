from django.db import models
class Programme(models.Model):
    libelle=models.CharField(max_length=255)
    annee=models.IntegerField()
    serie=models.CharField(max_length=255)
    poids=models.IntegerField()
    def __str__(self):
        return self.libelle
    