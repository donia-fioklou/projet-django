from django.db import models

from learnApp.models.categorieProduit import CategorieProduit

class Produit(models.Model):
    categorie=models.ForeignKey(CategorieProduit,on_delete=models.CASCADE)
    nom = models.CharField(max_length=255)
    prix = models.IntegerField()
    description = models.TextField()
    

    def __str__(self):
        return self.nom